ayatana-webmail (22.6.28+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 03 Jul 2022 13:35:08 +0200

ayatana-webmail (22.2.26+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 26 Feb 2022 22:43:48 +0100

ayatana-webmail (21.11.17+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream/metadata:
    + Update points of contact, put UBports Foundation in Donation: field.
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.
  * debian/rules:
    + Use upstream's NEWS file as upstream ChangeLog.
  * debian/copyright:
    + Add auto-generated copyright.in file for tracking copyright changes during
      later uploads.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 02 Dec 2021 00:03:22 +0100

ayatana-webmail (21.2.6+dfsg1-1) unstable; urgency=medium

  * New upstream release.
    - Fix name clash with icons in ayatana-indicator-messages.
      (Closes: #982037).
  * debian/patches:
    + Drop 1001_fix-AyatanaAppIndicator-loading-on-non-ayatana-desktops.patch.
      Applied upstream.
  * debian/manpages:
    + Use upstream's man pages.
  * debian/man/*:
    + Drop maintainer man pages, now shipped upstream.
  * debian/copyright:
    + Update copyright attributions.
  * debian/postinst:
    + Drop file. Not useful anymore.
  * debian/{rules,links}:
    + Adapt to new icon file names.
  * debian/links:
    + Drop file, not needed anymore.
  * debian/rules:
    +  Adapt file location for permissiong fix.
    +  Move egg dir removal into dh_clean override target.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 06 Feb 2021 02:51:17 +0100

ayatana-webmail (21.1.25+dfsg1-2) unstable; urgency=medium

  * Source-only upload.

  * debian/man/ayatana-webmail-clear.1:
    + Improve description about what this command actually does.
  * debian/man/ayatana-webmail-reset.1:
    + Improve description about what this command actually does.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 03 Feb 2021 23:17:39 +0100

ayatana-webmail (21.1.25+dfsg1-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #981107).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 26 Jan 2021 15:34:57 +0100
